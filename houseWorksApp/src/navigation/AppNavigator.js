import React from 'react';

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faCogs, faHome, faTrophy } from '@fortawesome/free-solid-svg-icons';

import * as s from '../screens';
import theme from '../theme';


const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const isAuthenticated = true;
const hasHome = false;


// Main stack navigator
const MainStackNavigator = props => {

  return (
    <Stack.Navigator>
      <Stack.Screen name="Home" component={s.HomeScreen} />
    </Stack.Navigator>
  );
}

// LeaderBoard stack navigator
const LeaderBoardStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="LeaderBoard" component={s.LeaderBoardScreen} />
    </Stack.Navigator>
  );
}

// Settings stack navigator
const SettingsStackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Settings" component={s.SettingsScreen} />
    </Stack.Navigator>
  );
}

// Create/join Home stack navigator
const COJHomeStackNavigator = () => {
  return (
    <Stack.Navigator headerMode={false}>
      <Stack.Screen name="CreateHome" component={s.CreateHomeScreen} />
      <Stack.Screen name="JoinHome" component={s.JoinHomeScreen} />
    </Stack.Navigator>
  );
}

// Authentification stack navigator
const AuthStackNavigator = () => {
  return (
    <Stack.Navigator headerMode={false}>
      <Stack.Screen name="Login" component={s.LoginScreen} />
      <Stack.Screen name="Register" component={s.RegisterScreen} />
    </Stack.Navigator>
  );
}

// Main tab navigator
const TabNavigator = () => {
  return (
    <Tab.Navigator
      tabBarOptions={{
        showLabel: false,
        activeTintColor: theme.PRIMARY_COLOR
      }}
    >
      {hasHome ?
        (<>
          <Tab.Screen
            name="Main"
            component={MainStackNavigator}
            options={{
              tabBarIcon: ({ color, size }) => (
                <FontAwesomeIcon color={color} icon={faHome} size={size} />
              )
            }}
          />
          <Tab.Screen
            name="LeaderBoard"
            component={LeaderBoardStackNavigator}
            options={{
              tabBarIcon: ({ color, size }) => (
                <FontAwesomeIcon color={color} icon={faTrophy} size={size} />
              )
            }}
          /></>
        )
        :
        (
          <Tab.Screen
            name="Main"
            component={COJHomeStackNavigator}
            options={{
              tabBarIcon: ({ color, size }) => (
                <FontAwesomeIcon color={color} icon={faHome} size={size} />
              )
            }}
          />
        )

      }

      <Tab.Screen
        name="Settings"
        component={SettingsStackNavigator}
        options={{
          tabBarIcon: ({ color, size }) => (
            <FontAwesomeIcon color={color} icon={faCogs} size={size} />
          )
        }}
      />
    </Tab.Navigator>
  );
}

const AppNavigator = isAuthenticated ? TabNavigator : AuthStackNavigator;

export default AppNavigator;

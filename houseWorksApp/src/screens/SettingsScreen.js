import React from 'react';
import { View, Text } from 'react-native';

import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';


const HomeScreen = ({ navigation }) => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <Text>Settings Screen</Text>
      </View>
    </View>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: theme.BACKGROUND_COLOR,
    flex: 1,
  },
  subContainer: {
    marginHorizontal: theme.DEFAULT_SPACING,
    flex: 1,
  }
});

export default HomeScreen;
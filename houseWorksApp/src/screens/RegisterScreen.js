import React from 'react';
import { SafeAreaView, View, Image, KeyboardAvoidingView } from 'react-native';
import { useForm } from 'react-hook-form';

import { faArrowLeft, faArrowRight, faRegistered, faSignInAlt } from '@fortawesome/free-solid-svg-icons';
import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';
import { Logo } from '../image';

import ButtonText from '../components/ButtonText';
import TextInputForm from '../components/TextInputForm';
import Separator from '../components/Separator';


const RegisterScreen = ({ navigation }) => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    console.log(data);
    props.toggleVisible(false);
  }

  return (
    <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={{ flex: 1 }}>
        <View style={styles.subContainer}>
          <View style={styles.logoContainer}>
            <Image style={styles.logo} source={Logo} />
          </View>
          <Separator/>
          <TextInputForm placeholder="Email" name="email" errors={errors} control={control} keyboardType="email-address" />
          <Separator/>
          <TextInputForm placeholder="Password" name="password" errors={errors} control={control} secureTextEntry />
          <Separator/>
          <TextInputForm placeholder="Confirm Password" name="confirmPassword" errors={errors} control={control} secureTextEntry />
          <Separator/>
          <ButtonText  text="Register" onPress={handleSubmit(onSubmit)} />
          <Separator/>
          <ButtonText icon={faArrowLeft} text="Sign-in" color={null} onPress={() => navigation.goBack()} />
        </View>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: theme.BACKGROUND_COLOR,
    flex: 1,
  },
  subContainer: {
    margin: theme.DEFAULT_SPACING,
    flex: 1
  },
  logoContainer: {
    flex: 1,
  },
  logo: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'contain'
  },
  separator: {
    marginVertical: theme.LIST_SPACING,
  },
});

export default RegisterScreen;
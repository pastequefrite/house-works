import React from 'react';
import { View, Modal, Text, SafeAreaView, KeyboardAvoidingView } from 'react-native';
import { useForm } from 'react-hook-form';

import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';

import theme from '../theme';

import PropTypes from 'prop-types';

import ButtonText from './ButtonText';
import TextInputForm from './TextInputForm';
import Separator from './Separator';


const EditModal = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const { control, handleSubmit, formState: { errors } } = useForm();
  const onSubmit = data => {
    console.log(data);
    props.toggleVisible(false);
  }


  return (
    <Modal
      animationType="slide"
      visible={props.visible}
      presentationStyle="overFullScreen"
    >
      <SafeAreaView style={styles.container}>
      <KeyboardAvoidingView behavior={Platform.OS === "ios" ? "padding" : "height"} style={{ flex: 1 }}>
        <View style={styles.header}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
        <View style={styles.subContainer}>
          <TextInputForm placeholder="Title" name="title" errors={errors} control={control} />
          <Separator />
          <TextInputForm placeholder="Point(s)" name="points" errors={errors} control={control} />
          <Separator />
          <ButtonText color={theme.PRIMARY_COLOR} text="Submit" onPress={handleSubmit(onSubmit)} />
          <Separator />
          <ButtonText color={theme.SECONDARY_COLOR} text="Cancel" onPress={props.toggleVisible} />
        </View>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </Modal>
  );
};

EditModal.propTypes = {
  visible: PropTypes.bool.isRequired,
  toggleVisible: PropTypes.func.isRequired,
  title: PropTypes.string,
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
    backgroundColor: theme.BACKGROUND_COLOR,
    flex: 1,
  },
  subContainer: {
    margin: theme.DEFAULT_SPACING,
  },
  header: {
    justifyContent: 'center',
    paddingBottom: 10,
  },
  title: {
    color: theme.FONT_COLOR,
    fontSize: theme.FONT_SIZE_LARGE,
    textAlign: 'center',
  }
});

export default EditModal;

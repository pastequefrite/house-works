import React from 'react';
import { StyleSheet, TouchableOpacity, Text, View } from 'react-native';
import PropTypes from 'prop-types';

import theme from '../theme';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';


const ButtonText = props => {

    const styleFromPropsContainer = {};
    const styleFromPropsText = {};

    if (props.color) {
        styleFromPropsContainer.backgroundColor = props.color;
    } else if (props.color === undefined) {
        styleFromPropsContainer.backgroundColor = theme.PRIMARY_COLOR;
    }

    if (props.textSize) {
        styleFromPropsText.fontSize = props.textSize;
    } else {
        styleFromPropsText.fontSize = theme.FONT_SIZE_MEDIUM;
    }

    if (props.radius) {
        styleFromPropsContainer.borderRadius = props.radius;
    } else {
        styleFromPropsContainer.borderRadius = theme.DEFAULT_BUTTON_RADIUS;
    }

    if (props.height) {
        styleFromPropsContainer.height = props.height;
    } else {
        styleFromPropsContainer.height = 50;
    }

    return (
        <View style={[props.disabled && styles.disabled, styles.container]}>
            <TouchableOpacity style={[styles.subContainer, styleFromPropsContainer]} onPress={props.onPress} disabled={props.disabled}>
                {props.iconRight && (<Text style={[styles.text, styleFromPropsText]}>{props.text}</Text>)}
                {props.icon && (<FontAwesomeIcon style={styles.icon} icon={props.icon} color={theme.WHITE_COLOR} size={props.iconSize ? props.iconSize : theme.ICON_SIZE_SMALL} />)}
                {!props.iconRight && (<Text style={[styles.text, styleFromPropsText]}>{props.text}</Text>)}
            </TouchableOpacity>
        </View>
    );
};

ButtonText.propTypes = {
    onPress: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    textSize: PropTypes.number,
    icon: PropTypes.object,
    iconSize: PropTypes.number,
    color: PropTypes.string,
    radius: PropTypes.number,
    disabled: PropTypes.bool,
    height: PropTypes.number,
    iconRight: PropTypes.bool

}

const styles = StyleSheet.create({
    container: {
        //flex: 1
    },
    subContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
        padding: 5,
    },
    icon: {
        marginHorizontal: 10
    },
    text: {
        color: theme.WHITE_COLOR,
        fontWeight: theme.FONT_WEIGHT_MEDIUM,
    },
    disabled: {
        opacity: 0.5
    }
});

export default ButtonText;

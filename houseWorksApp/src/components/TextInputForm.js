import React from 'react';
import { View, Text, TextInput } from 'react-native';

import { Controller, useForm } from 'react-hook-form';

import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';

import theme from '../theme';

import PropTypes from 'prop-types';


const TextInputForm = props => {
  const styles = useDynamicStyleSheet(dynamicStyles);

  const errorInput = {
    borderColor: theme.RED_COLOR,
    borderWidth: 1
  }

  return (
        <View style={styles.container}>
          <Text style={styles.label}>{props.placeholder} :</Text>
          <Controller
            control={props.control}
            render={({ field: { onChange, onBlur, value } }) => (
              <TextInput
                style={[styles.input, props.errors[props.name] && errorInput]}
                onBlur={onBlur}
                onChangeText={value => onChange(value)}
                value={value}
                placeholder={props.placeholder}
                placeholderTextColor={theme.SECONDARY_COLOR}
                keyboardType={props.keyboardType}
                autoCorrect={props.autoCorrect ? props.autoCorrect : false}
                secureTextEntry={props.secureTextEntry}
              />
            )}
            name={props.name}
            rules={{ required: true }}
            defaultValue={props.defaultValue}
          />
          <Text style={styles.error}>{props.errors[props.name] && props.placeholder + " is required"} </Text>
        </View>
  );
};

TextInputForm.propTypes = {
  name: PropTypes.string.isRequired,
  placeholder: PropTypes.string.isRequired,
  defaultValue: PropTypes.string,
  errors: PropTypes.object.isRequired, 
  control: PropTypes.object.isRequired,
  autoCorrect: PropTypes.bool,
  keyboardType: PropTypes.string,
  secureTextEntry: PropTypes.bool
};

const dynamicStyles = new DynamicStyleSheet({
  container: {
  },
  input: {
    backgroundColor: theme.BACKGROUND_SOFT_COLOR,
    height: 50,
    paddingHorizontal: 10 ,
    borderRadius: 10,
    color: theme.FONT_COLOR,
    fontSize: theme.FONT_SIZE_MEDIUM
  },
  label: {
    color: theme.FONT_COLOR,
    fontSize: theme.FONT_SIZE_MEDIUM,
    marginBottom: 5,
  },
  error: {
    color: theme.RED_COLOR,
    fontSize: theme.FONT_SIZE_MEDIUM,
    marginTop: 5
  }
});

export default TextInputForm;
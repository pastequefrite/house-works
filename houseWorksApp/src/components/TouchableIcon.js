import React from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { useDynamicStyleSheet, DynamicStyleSheet } from 'react-native-dynamic';
import theme from '../theme';



const TouchableIcon = props => {
    const styles = useDynamicStyleSheet(dynamicStyles);

    return (
        <TouchableOpacity style={props.style} onPress={props.onPress}>
            <FontAwesomeIcon icon={props.icon} color={props.color ? props.color : styles.iconColor.color} />
        </TouchableOpacity>
    );
};

TouchableIcon.propTypes = {
    onPress: PropTypes.func.isRequired,
    icon: PropTypes.object.isRequired,
    color: PropTypes.string,
    style: PropTypes.object
}

const dynamicStyles = new DynamicStyleSheet({
    iconColor: {
        color: theme.ICON_COLOR,
    }
});

export default TouchableIcon;
